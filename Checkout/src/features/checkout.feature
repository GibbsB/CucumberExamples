Feature: Checkout

  Scenario Outline: Checkout bananas and apples
    Given the price of a "banana" is 40c
    And the price of a "apple" is 25c
    When I checkout "banana" <bananaCount>
    And I checkout "apple" <appleCount>
    Then the total price should be <total>c
    Examples:
      | bananaCount | appleCount | total |
      | 1           | 1          | 65    |
      | 1           | 2          | 90    |
      | 2           | 1          | 105   |
      | 0           | 3          | 75    |
      | -1          | -2         | 0     |