package features;

import cucumber.api.CucumberOptions;
import cucumber.api.junit.Cucumber;
import org.junit.runner.RunWith;

/**
 * Skeleton class to run tests and return the output into the console.
 */
@RunWith(Cucumber.class)
@CucumberOptions(
        format = "pretty"
)
public class CheckoutTest {
}
