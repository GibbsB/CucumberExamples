package features.step_definitions;

import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import features.implementation.Checkout;
import org.junit.Assert;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

public class CheckoutSteps {
    private Checkout checkout = new Checkout();

    @Given("^the price of a \"([^\"]*)\" is (\\d+)c$")
    public void thePriceOfAIsC(String name, int price) {
        checkout.addItem(name, price);
    }

    /**
     * Catches both the When and And "I checkout ..." from feature file.
     * Allows negative numbers to make sure Checkout doesn't allow negative counts.
     * An un-checkout functionality would be implemented in a real system
     * but this Checkout doesn't keep track of items added (only the total price)
     * @param name name of the item
     * @param count count of the item to be checked-out
     */
    @When("^I checkout \"([^\"]*)\" (-?\\d+)$")
    public void iCheckout(String name, int count) {
        if (count < 0)
            assertFalse("Allowed " + count + " value when negative values are not allowed", checkout.checkout(name, count));
        else
            assertTrue(name + " was not recognized in the list of items", checkout.checkout(name, count));
    }

    /**
     * Checks the expected price with the actual
     * @param total total price of the items in c
     */
    @Then("^the total price should be (\\d+)c$")
    public void theTotalPriceShouldBeTotalC(int total) {
        Assert.assertEquals("Expected total: "+ total + " does not equal " +
            checkout.getCurrentTotal(), total, checkout.getCurrentTotal());
    }
}
