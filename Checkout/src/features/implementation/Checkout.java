package features.implementation;

import java.util.LinkedHashMap;

/**
 * Keeps the total price of items checked-out.
 * Does not keep track of items checked-out. A real system with higher complexity would.
 */
public class Checkout {
    private LinkedHashMap<String, Integer> items = new LinkedHashMap<>();
    private int currentTotal;

    public Checkout() {
        currentTotal = 0;
    }

    public int getCurrentTotal() {
        return currentTotal;
    }

    /**
     * Searches the item in the checkout list and adds the total cost to the total sum.
     * @param name name of item
     * @param count count of items
     * @return true if the item(s) was successfully checked-out
     */
    public boolean checkout(String name, int count) {
        if (count < 0)
            return false;

        Integer itemPrice = items.get(name);
        if (itemPrice == null)
            return false;    // item not found

        currentTotal += (itemPrice * count);
        return true;
    }

    /**
     * Adds an item and price to the Checkout object which can be checked-out later with the
     * checkout(String, int) method.
     * @param name name of item
     * @param price price of item in c
     * @return true if item was successfully added to the items collection
     */
    public boolean addItem(String name, int price) {
        return items.put(name, price) != null;
    }
}
